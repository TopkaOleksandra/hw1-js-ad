//Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//при прототипному наслідуванні відбувається успадкування об'єктами властивостей прототипа, тобто об'єкт базується на іншому об'єкті.
//Для чого потрібно викликати super() у конструкторі класу-нащадка?
//для того щоб побудувати метод у нащадка на базі батьківського, наприклад розширити функціональність батьківського метода у класі нащадка.
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }
    set name(name) {
        return (this._name = name);
    }

    get age() {
        return this._age;
    }
    set age(age) {
        return (this._age = age);
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        return (this._salary = value);
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary () {
        return super.salary * 3;
    }

}

const junior = new Programmer ("Alex", 25, 200, "UK")

const middle = new Programmer ("Nic", 30, 500, "UK, PL")


console.log(junior);
console.log(middle);
console.log(middle.salary);
